#!/bin/bash

echo "Starting Restore..."

# Backup path
BACKUP_PATH="/backup-temp/dump/"

# Copy to Google Cloud Storage
echo "Copying gs://$BUCKET/$BACKUP_FILENAME" to $BACKUP_PATH
/root/gsutil/gsutil cp gs://"$BUCKET"/"$BACKUP_FILENAME" "$BACKUP_PATH" 2>&1
echo "Copying finished"

# Decompress
tar -xvzf "$BACKUP_PATH""$BACKUP_FILENAME"

# Delete
rm "$BACKUP_PATH""$BACKUP_FILENAME"

# Get DB Hosts
echo "Getting MongoDB Hosts..."
DB_HOST=($(curl http://pod-ip-finder/string/?role=mongo))

# Restore the back up
echo "Connecting to MongoDB Hosts ${DB_HOST}"
mongorestore -h "${DB_HOST}" "$BACKUP_PATH"

# Remove the restore file
echo "Removing backup data"
rm -rf $BACKUP_PATH*