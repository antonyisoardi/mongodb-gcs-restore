FROM ubuntu:14.04
MAINTAINER Antony Isoardi <antonyisoardi@gmail.com>
LABEL Description="Mongodb cron backup to Google Cloud Storage (GCE) - Modified from Jadson Lourenço's Mongodb cron backup"

RUN apt-get update && \
  apt-get install -y software-properties-common python-software-properties curl

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 && \
  echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list


#RUN add-apt-repository ppa:fkrull/deadsnakes && \
#  apt-get update && \
#  apt-get install -y --force-yes mongodb-org-tools && \
#  apt-get install -y python3.5 mercurial && \
#  apt-get install -y python-pip

RUN apt-get update && apt-get install -y --force-yes mongodb-org-tools

RUN curl -s -O https://storage.googleapis.com/pub/gsutil.tar.gz && \
  tar xfz gsutil.tar.gz -C $HOME && \
  chmod 777 /root/gsutil && chmod 777 /root/gsutil/* && \
  rm gsutil.tar.gz

COPY ./mongodb-restore.sh /
RUN chmod +x /mongodb-restore.sh

COPY ./entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]